"use strict";

require("sugar");

const crypto = require("crypto");
const express  = require("express");
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server);

const port = 2002;
server.listen(port, function () {
  console.log('Server listening at port %d', port);
});

const mqtt = require("mqtt");

app.use(express.static(__dirname + '/static'));

const tasks = {};

const mqttClient = mqtt.connect('mqtt://52.30.19.155', {clientId: 'bgtestnodejs', protocolId: 'MQIsdp', protocolVersion: 3, connectTimeout:1000, debug:true});
mqttClient.on("connect", () => {
    console.log("connected to mqtt://52.30.19.155:1883");
    mqttClient.subscribe('foo');
});

mqttClient.on("message", (topic, message) => {
    console.log("Recieved a message from MQTT: ", message.toString());

    const msg = JSON.parse(message.toString());
    if (!msg.hasOwnProperty("success") || !msg.hasOwnProperty("payload")) {
        return;
    }
    const socketTopic = msg["request"]["className"] === "Forecast$" ? "forecast" : "info"
    tasks[msg["request"]["external_id"]].emit(socketTopic, msg);
    delete tasks[msg["request"]["external_id"]];
});

mqttClient.on("error", (error) => {
    console.error("mqtt error", error);
});

io.on('connection', (socket) => {
    const ID = (socket.id).toString();
    console.log("Connected new WS client: ", ID);    

    socket.on('get forecast', (msg) => {
        console.log(`Recieved a message from client#${ID}: `, msg);
        const hash = crypto.createHash('sha1').update(new Date().getTime().toString() + Math.random()).digest('hex');

        const request = {
            "jarPath": "/home/ubuntu/mist_tests/target/scala-2.10/mist_tests_2.10-0.0.1.jar",
            "className": "Forecast$",
            "name": "bar",
            "parameters": {
                "meter_id": msg['meter_id']
            },
            "external_id": hash
        };
        tasks[hash] = socket;
        mqttClient.publish("foo", JSON.stringify(request));
    });

    socket.on('get info', (msg) => {
        console.log(`Recieved a message from client#${ID}: `, msg);

                const hash = crypto.createHash('sha1').update(new Date().getTime().toString() + Math.random()).digest('hex');

        const request = {
            "jarPath": "/home/ubuntu/mist_tests/target/scala-2.10/mist_tests_2.10-0.0.1.jar",
            "className": "Info$",
            "name": "bar",
            "parameters": {
                "meter_id": msg['meter_id']
            },
            "external_id": hash
        };
        tasks[hash] = socket;
        mqttClient.publish("foo", JSON.stringify(request));
    });
});
