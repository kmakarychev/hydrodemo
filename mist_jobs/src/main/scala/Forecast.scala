import io.hydrosphere.mist.MistJob
import org.apache.spark.sql.hive.HiveContext
import math._
import com.github.nscala_time.time.Imports._

object Forecast extends MistJob {
  override def doStuff(hc: HiveContext, parameters: Map[String, Any]) = {
    val N = 200

    val meterId = parameters("meter_id")
    val feederRows = hc.sql(s"SELECT feeder FROM meters WHERE Meter_ID = '${meterId}'").collect()
    val FEEDER = feederRows(0)(0)

    val now = new DateTime("2015-11-02T00:00:00.000")
    val nowTimestamp = (now.getMillis() / 1000).toInt
    val days = (ceil((N - now.getDayOfWeek() - 1f) / 5f) * 2f + N).toInt
    val fromDate = now - (days).days
    val fromTimestamp = (fromDate.getMillis() / 1000).toInt
    val tomorrowTimestamp = ((now + 1.days).getMillis() / 1000).toInt

    hc.sql(s"SELECT kwh, local_timestamp, from_unixtime(local_timestamp,'u') AS local_dayofweek, ((local_timestamp / (15 * 60)) % (24 * 4)) AS quarter_number FROM denormalized_kwh WHERE feeder = $FEEDER").registerTempTable("new_kwh")
    val result = hc.sql(s"SELECT (quarter_number * 15 * 60 + ${nowTimestamp}) as new_date, quarter_number, AVG(kwh) as kwh FROM new_kwh WHERE local_timestamp >= ${fromTimestamp} AND local_timestamp < ${nowTimestamp} and local_dayofweek NOT IN (1, 7) GROUP BY quarter_number").collect().map(x => Map(("quarter_number", x.getAs[Int]("quarter_number")), ("kwh", x.getAs[Double]("kwh")), ("new_date", x.getAs[Int]("new_date"))))

    Map("result" -> result)
  }
}
