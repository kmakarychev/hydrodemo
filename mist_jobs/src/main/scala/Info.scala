import io.hydrosphere.mist.MistJob
import org.apache.spark.sql.SQLContext

object Info extends MistJob {
  override def doStuff(context: SQLContext, parameters: Map[String, Any]): Map[String, Any] = {
    val meterId = parameters("meter_id").asInstanceOf[String]
    val df = context.read.parquet(s"hdfs://ip-172-30-2-18.eu-west-1.compute.internal/user/hive/warehouse/denormalized_kwh_by_meter/meter_id=${meterId}")
    df.registerTempTable("meter")
    val result = context.sql(s"""SELECT local_timestamp, kwh FROM meter ORDER BY local_timestamp LIMIT 96""").collect().map { x => Map(("local_timestamp", x.getAs[Double]("local_timestamp")), ("kwh", x.getAs[Int]("kwh"))) }
    Map("result" -> result)
  }
}
